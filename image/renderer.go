// Package image func for rendering and loading images
package image

import (
	"image"

	"github.com/nfnt/resize"

	gfx "gitlab.com/nikkofox/gomeme-api/image/draw"
	"gitlab.com/nikkofox/gomeme-api/image/stream"
	"gitlab.com/nikkofox/gomeme-api/models"
)

const (
	maxImageSize = 1400 // px
)

// RenderImage performs the graphical manipulation of the image.
func RenderImage(meme models.Meme, st stream.Stream) (stream.Stream, error) {
	st, err := renderImage(meme, st)
	return st, err
}

// RenderImage performs the graphical manipulation of the image.
func renderImage(meme models.Meme, st stream.Stream) (stream.Stream, error) {
	img, err := st.DecodeImage()
	if err != nil {
		return stream.Stream{}, err
	}
	img = reduceImage(img, maxImageSize)

	// Draw on the text.
	ctx := gfx.NewContext(img)
	if meme.TopText != "" {
		gfx.TopBanner(ctx, meme.TopText, meme.FastStroke)
	}
	if meme.BottomText != "" {
		gfx.BottomBanner(ctx, meme.BottomText, meme.FastStroke)
	}

	return stream.EncodeImage(ctx.Image())
}

// reduceImage will resize an image if any of its dimensions are above the passed max size.
func reduceImage(img image.Image, maxSize uint) image.Image {
	w := img.Bounds().Dx()
	h := img.Bounds().Dy()

	if w > h && w > int(maxSize) {
		img = resize.Resize(maxSize, 0, img, resize.Lanczos2)
	} else if h > w && h > int(maxSize) {
		img = resize.Resize(0, maxSize, img, resize.Lanczos2)
	}

	return img
}
