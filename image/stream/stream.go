// Package stream work with io.Reader
package stream

import (
	"bytes"
	"image"
	"image/jpeg"
	"io"
	"io/ioutil"
)

const (
	jpqQuality = 85 // 0-100
)

// Stream contains information about a loaded image.
type Stream struct {
	io.Reader
	bytes []byte
	index int
	typ   string
}

// Bytes returns the stream's bytes.
func (st *Stream) Bytes() []byte {
	return st.bytes
}

// Read implements the io.Reader interface for the Stream.
func (st *Stream) Read(b []byte) (n int, err error) {
	if st.index >= len(st.bytes) {
		return 0, io.EOF
	}
	n = copy(b, st.bytes[st.index:])
	st.index += n
	return
}

// IsGif returns true if the loaded image is a gif.
func (st *Stream) IsGif() bool {
	return st.typ == "gif"
}

// IsJpg returns true if the loaded image is a Jpeg.
func (st *Stream) IsJpg() bool {
	return st.typ == "jpeg"
}

// IsPng returns true if the loaded image is a Png.
func (st *Stream) IsPng() bool {
	return st.typ == "png"
}

// FileExt returns the file extension of the image.
func (st *Stream) FileExt() string {
	if st.IsGif() {
		return "gif"
	}
	if st.IsJpg() {
		return "jpg"
	}
	if st.IsPng() {
		return "png"
	}
	return "unknown"
}

// NewStream creates a new stream struct.
func NewStream(s io.Reader) (Stream, error) {
	a, err := ioutil.ReadAll(s)
	if err != nil {
		return Stream{}, err
	}

	b := make([]byte, len(a))
	copy(b, a)

	_, typ, err := image.DecodeConfig(bytes.NewReader(a))
	if err != nil {
		return Stream{}, err
	}
	return Stream{
		bytes: b,
		typ:   typ,
	}, err
}

// EncodeImage encodes an image into a stream.
func EncodeImage(img image.Image) (Stream, error) {
	var buffer bytes.Buffer
	_ = jpeg.Encode(&buffer, img, &jpeg.Options{Quality: jpqQuality})
	return NewStream(&buffer)
}

// DecodeImage decodes the byte stream and returns an image.
func (st *Stream) DecodeImage() (image.Image, error) {
	img, _, err := image.Decode(st)
	return img, err
}
