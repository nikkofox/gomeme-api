// Package services ??? just because
package services

import (
	"gitlab.com/nikkofox/gomeme-api/image"
	"gitlab.com/nikkofox/gomeme-api/image/stream"
	"gitlab.com/nikkofox/gomeme-api/models"
)

// CreateMeme loads the image into the stream and renders the font
func CreateMeme(meme *models.Meme) (stream.Stream, error) {
	st, err := image.Load(*meme)
	if err == nil {
		st, err = image.RenderImage(*meme, st)
	}
	return st, err
}
