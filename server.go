package main

import (
	"io"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"gopkg.in/natefinch/lumberjack.v2"

	"gitlab.com/nikkofox/gomeme-api/controllers"
)

type customValidator struct {
	validator *validator.Validate
}

// Validate used as echo.Validator for structure validation
func (cv *customValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	mw := io.MultiWriter(os.Stdout, &lumberjack.Logger{
		Filename:   "log/gomeme-api.log",
		MaxSize:    5,
		MaxBackups: 20,
		MaxAge:     90,
		Compress:   true,
	})

	e := echo.New()
	e.Logger.SetLevel(log.DEBUG)
	e.Logger.SetOutput(mw)
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "[${time_rfc3339}] - ${remote_ip} \"${method} ${uri}\" ${status} (${error}) ${latency_human} ${bytes_in}:${bytes_out}\n",
	}))
	e.Use(middleware.Recover())
	e.Use(middleware.BodyLimit("6M"))

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.POST},
	}))

	e.Validator = &customValidator{validator: validator.New()}

	g := e.Group("/meme")
	g.POST("/create", controllers.CreateHandler)

	e.Logger.Fatal(e.Start(":7744"))
}
