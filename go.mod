module gitlab.com/nikkofox/gomeme-api

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/fogleman/gg v1.3.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/labstack/echo/v4 v4.2.0
	github.com/labstack/gommon v0.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/stretchr/testify v1.7.0
	golang.org/x/image v0.0.0-20210220032944-ac19c3e999fb // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
