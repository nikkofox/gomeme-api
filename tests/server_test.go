package main_test

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nikkofox/gomeme-api/controllers"
)

type customValidator struct {
	validator *validator.Validate
}

func (cv *customValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

type echoTest struct {
	base *echo.Echo
}

func (e *echoTest) setup() {
	e.base = echo.New()
	e.base.Validator = &customValidator{validator: validator.New()}
}

func (e *echoTest) jsonRequest(payload string) (echo.Context, *httptest.ResponseRecorder) {
	req := httptest.NewRequest(http.MethodPost, "/meme/create", strings.NewReader(payload))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	return e.base.NewContext(req, rec), rec
}

func (e *echoTest) formRequest(payload io.Reader, contentType string) (echo.Context, *httptest.ResponseRecorder) {
	req := httptest.NewRequest(http.MethodPost, "/meme/create", payload)
	req.Header.Set(echo.HeaderContentType, contentType)
	rec := httptest.NewRecorder()
	return e.base.NewContext(req, rec), rec
}

var et = new(echoTest)

func TestMain(m *testing.M) {
	et.setup()
	os.Exit(m.Run())
}

func TestCreateMemeJSONOK(t *testing.T) {
	// Setup url image test
	resultHash := "87a6de6c607fedc5ac97ebb88b1d01fc"
	jsonStr := `{"image": "https://picsum.photos/id/118/1500/1000", "top_text": "its", "bottom_text": "meme"}`
	c, rec := et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup path image test
	resultHash = "39234eea493eeb21e0f6843783318407"
	jsonStr = `{"image": "assets/druzhko.jpg", "top_text": "its", "bottom_text": "meme"}`
	c, rec = et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup path image test with fast_stroke
	resultHash = "d11c0c4dac45b98264a5aa56c9065c42"
	jsonStr = `{"image": "assets/druzhko.jpg", "top_text": "its", "bottom_text": "meme", "fast_stroke": true}`
	c, rec = et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup dataURI image test
	resultHash = "07f253601edadb6a00fcf968016daf79"
	file, _ := ioutil.ReadFile("assets/druzhko.jpg")
	base64Img := base64.StdEncoding.EncodeToString(file)
	jsonStr = `{"image": "` + `data:image/jpg;base64,` + base64Img + `", "top_text": "datauri", "bottom_text": "image"}`
	c, rec = et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup empty text test
	resultHash = "b5907bc72f3c7fd95d32dbf1e06c6a2b"
	jsonStr = `{"image": "assets/druzhko.jpg", "top_text": "", "bottom_text": ""}`
	c, rec = et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup without text fields test
	resultHash = "b5907bc72f3c7fd95d32dbf1e06c6a2b"
	jsonStr = `{"image": "assets/druzhko.jpg"}`
	c, rec = et.jsonRequest(jsonStr)

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}
}

func TestCreateMemeJSONNotOK(t *testing.T) {
	// Setup incorrect image field
	jsonStr := `{"image": "incorrect image", "top_text": "its", "bottom_text": "meme"}`
	c, _ := et.jsonRequest(jsonStr)

	// Assertions
	err := controllers.CreateHandler(c)
	if assert.NotNil(t, err) {
		he, ok := err.(*echo.HTTPError)
		if ok {
			assert.Equal(t, http.StatusBadRequest, he.Code)
			assert.Equal(t, "Bad request param: `image`", he.Message)
		}
	}

	// Setup incorrect json
	jsonStr = `incorrect payload`
	c, _ = et.jsonRequest(jsonStr)

	// Assertions
	err = controllers.CreateHandler(c)
	if assert.NotNil(t, err) {
		he, ok := err.(*echo.HTTPError)
		if ok {
			assert.Equal(t, http.StatusBadRequest, he.Code)
			assert.Equal(t, "Bad request param: `image`", he.Message)
		}
	}
}

func TestCreateMemeFormFileOK(t *testing.T) {
	// Setup form file test
	resultHash := "39234eea493eeb21e0f6843783318407"
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	file, _ := os.Open("assets/druzhko.jpg")
	defer file.Close()
	part, _ := writer.CreateFormFile("image", filepath.Base("assets/druzhko.jpg"))
	io.Copy(part, file)

	_ = writer.WriteField("top_text", "its")
	_ = writer.WriteField("bottom_text", "meme")
	writer.Close()

	c, rec := et.formRequest(payload, writer.FormDataContentType())

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}

	// Setup form file test with fast_stroke
	resultHash = "d11c0c4dac45b98264a5aa56c9065c42"
	payload = &bytes.Buffer{}
	writer = multipart.NewWriter(payload)
	file, _ = os.Open("assets/druzhko.jpg")
	defer file.Close()
	part, _ = writer.CreateFormFile("image", filepath.Base("assets/druzhko.jpg"))
	io.Copy(part, file)

	_ = writer.WriteField("top_text", "its")
	_ = writer.WriteField("bottom_text", "meme")
	_ = writer.WriteField("fast_stroke", "true")
	writer.Close()

	c, rec = et.formRequest(payload, writer.FormDataContentType())

	// Assertions
	if assert.NoError(t, controllers.CreateHandler(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, resultHash, GetMD5Hash(rec.Body.Bytes()))
	}
}

func TestCreateMemeFormFileNotOK(t *testing.T) {
	// Setup
	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	_ = writer.WriteField("image", "incorrect image")
	_ = writer.WriteField("top_text", "its")
	_ = writer.WriteField("bottom_text", "meme")
	writer.Close()

	c, _ := et.formRequest(payload, writer.FormDataContentType())

	// Assertions
	err := controllers.CreateHandler(c)
	if assert.NotNil(t, err) {
		he, ok := err.(*echo.HTTPError)
		if ok {
			assert.Equal(t, http.StatusBadRequest, he.Code)
			assert.Equal(t, "Bad request param: `image`", he.Message)
		}
	}
}

func GetMD5Hash(file []byte) string {
	hasher := md5.New()
	hasher.Write(file)
	return hex.EncodeToString(hasher.Sum(nil))
}
