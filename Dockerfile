FROM golang:alpine as build-env
# All these steps will be cached
RUN mkdir /gomeme-api
WORKDIR /gomeme-api
COPY go.mod .
COPY go.sum .

# Get dependancies - will also be cached if we won't change mod/sum
RUN go mod download
# COPY the source code as the last step
COPY . .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/gomeme-api
FROM scratch
COPY --from=build-env /go/bin/gomeme-api /go/bin/gomeme-api
COPY --from=build-env /tmp /tmp
ENTRYPOINT ["/go/bin/gomeme-api"]
EXPOSE 7744