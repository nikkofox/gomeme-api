// Package controllers contains route handlers
package controllers

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/nikkofox/gomeme-api/models"
	"gitlab.com/nikkofox/gomeme-api/services"
)

// CreateHandler processes route /meme/create
func CreateHandler(c echo.Context) (imageFieldErr error) {
	meme := new(models.Meme)
	imageFieldErr = echo.NewHTTPError(http.StatusBadRequest, "Bad request param: `image`")

	cType := c.Request().Header.Get(echo.HeaderContentType)
	if strings.HasPrefix(cType, echo.MIMEApplicationForm) ||
		strings.HasPrefix(cType, echo.MIMEMultipartForm) {
		meme.TopText = c.FormValue("top_text")
		meme.BottomText = c.FormValue("bottom_text")
		meme.FastStroke, _ = strconv.ParseBool(c.FormValue("fast_stroke"))

		file, err := c.FormFile("image")
		if err != nil {
			return
		}
		meme.ImageForm, err = file.Open()
		if err != nil {
			return
		}
		defer func() {
			_ = meme.ImageForm.Close()
		}()
	} else if err := c.Bind(meme); err != nil {
		return
	}
	if err := c.Validate(meme); err != nil {
		return
	}
	stream, err := services.CreateMeme(meme)
	if err != nil {
		return
	}
	return c.Stream(http.StatusOK, "image/jpeg", &stream)
}
