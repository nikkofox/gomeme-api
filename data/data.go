//nolint
// Package data contains data assets to be compiled
package data

import (
	"embed"
	"fmt"
	"os"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Font is the location of the built-in font.
var Font = fmt.Sprint("fonts/", getEnv("FONT", "impact.ttf"))

//go:embed fonts/*
var Files embed.FS
