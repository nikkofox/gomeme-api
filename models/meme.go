// Package models contains structures
package models

import (
	"mime/multipart"
)

// Meme holds image data and text, FastStroke faster, but less accurate
type Meme struct {
	Image      string         `json:"image" validate:"required_without=ImageForm,omitempty,url|file|datauri"`
	ImageForm  multipart.File `form:"image" validate:"required_without=Image"`
	TopText    string         `json:"top_text" form:"top_text" validate:"omitempty"`
	BottomText string         `json:"bottom_text" form:"bottom_text" validate:"omitempty"`
	FastStroke bool           `json:"fast_stroke" validate:"omitempty"`
}
